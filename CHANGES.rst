Changelog of IAmReusable django app
===================================================

0.0.9 (unreleased)
------------------

- Nothing changed yet.


0.0.8 (2017-11-22)
------------------

- Changed to django 1.11.7


0.0.7 (2017-11-10)
------------------

- Removed bootstrap extension


0.0.6 (2017-11-10)
------------------

- Added MANIFEST.in


0.0.5 (2017-11-10)
------------------

- Changed to django 1.10.8

- Ready for python 3


0.0.4 (2017-11-10)
------------------

- Note: this need to be a rst, not a md file

0.0.3 (2017-11-9)
------------------

- Changes forgotten


0.0.2 (2017-11-09)
------------------

- Nothing changed


0.0.2 (2017-11-09)
------------------

- Starting an empty django app


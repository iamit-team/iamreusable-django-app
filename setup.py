from setuptools import setup

version = '0.0.9.dev0'

long_description = '\n\n'.join([
    open('README.md').read(),
    open('CREDITS.md').read(),
    open('CHANGES.rst').read(),
    ])

install_requires = [
    'Django',
    'django-extensions',
    ],

tests_require = [
    'iamreusable',
    ]

setup(name='iamreusable',
      version=version,
      description="TODO",
      long_description=long_description,
      classifiers=['Programming Language :: Python',
                   'Framework :: Django',
                   ],
      keywords=[],
      author='Michel van Leeuwen',
      author_email='michel@iamit.nl',
      url='http://iamit.nl',
      license='GPL',
      packages=['iamreusable'
                ],
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      tests_require=tests_require,
      extras_require = {'test': tests_require},
      entry_points={
          'console_scripts': [
          ]},
      )
